<?php

class Library {

    public function eligibleProvidersCsv($jsonFile, $csvFileName) {

        $jsonFile = file_get_contents($jsonFile);
        $data = json_decode($jsonFile, true);

        $csvFileName = fopen($csvFileName, 'w');
        $header = false;

        foreach ($data as $key => $row)
        {
            if (empty($header))
            {
                $header = array_keys($row);
                fputcsv($csvFileName, $header);
                $header = array_flip($header);
            }
            fputcsv($csvFileName, array_merge($header, $row));
        }
        fclose($csvFileName);
        return;
    }
}

$json_file = 'transactions.json';
$csv_filename = 'data.csv';
$converter = new Library();
$convert_to_csv = $converter->eligibleProvidersCsv($json_file, $csv_filename);
var_dump($convert_to_csv);

